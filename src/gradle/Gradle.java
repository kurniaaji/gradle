/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gradle;

/**
 *
 * @author kurnia
 */
class Thread1 extends Thread{
   @Override
    public void run() {
       for (int i = 5; i > 0; i--){
           System.out.println("Arif Menghitung : "+i);
           try {
               Thread.sleep(100);
           } catch (InterruptedException e) {
               e.printStackTrace();
           }
       }
       
       System.out.println("-------- Berhitung Selesai (Arif) --------");
    }
}

class Thread2 extends Thread{

    @Override
    public void run() {
        for (int i = 5; i > 0; i--){
            System.out.println("Aji Menghitung : "+i);
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        System.out.println("-------- Berhitung Selesai (Prastyo) --------");
    }
}

class Thread3 extends Thread{

    @Override
    public void run() {
        for (int i = 5; i > 0; i--){
            System.out.println("Prasetya Menghitung : "+i);
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        System.out.println("-------- Berhitung Selesai (Aji) --------");
    }
}
public class Gradle {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        Thread1 t1 = new Thread1();
        Thread2 t2 = new Thread2();
        Thread3 t3 = new Thread3();

        t1.start();
        t2.start();
        t3.start();


        try {
            t1.join();
            t2.join();
            t3.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        System.out.println("---------------- Proses SELESAI ----------------");
    }
        
}
    

