/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gradle;

/**
 *
 * @author kurnia
 */

class Multithreading1 implements Runnable 
{ 
    public void run() 
    { 
        try
        { 
            // Displaying the thread that is running 
            System.out.println ("Prastyo Menghitung : " + Thread.currentThread().getId());
  
        } 
        catch (Exception e) 
        { 
            // Throwing an exception 
            System.out.println ("Proses Gagal"+e); 
        } 
    } 
}

class Multithreading2 implements Runnable {
    public void run() 
    { 
        try
        { 
            // Displaying the thread that is running 
            System.out.println ("Arif Menghitung = " + Thread.currentThread().getId()); 
  
        } 
        catch (Exception e) 
        { 
            // Throwing an exception 
            System.out.println ("Proses Gagal"+e); 
        } 
    }
}

class Multithreading3 implements Runnable {
    public void run() 
    { 
        try
        { 
            // Displaying the thread that is running 
            System.out.println ("Prasetya Menghitung = " + Thread.currentThread().getId()); 
  
        } 
        catch (Exception e) 
        { 
            // Throwing an exception 
            System.out.println ("Proses Gagal"+e); 
        } 
    }
}
public class tread {
        public static void main(String[] args) throws InterruptedException 
    { 
        int n = 5; // Number of threads 
        for (int i = n; i > 0; i--) 
        { 
            Thread t1 = new Thread(new Multithreading1()); 
            Thread t2 = new Thread(new Multithreading3());
            Thread t3 = new Thread(new Multithreading2());
            t1.start();
            t2.start();
            t3.start();
            Thread.sleep(10);
        } 
    } 
}

